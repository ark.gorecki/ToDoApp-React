import React from 'react';

class AddItem extends React.Component{
	constructor(){
		super();
		this.onSubmit = this.onSubmit.bind(this);
	}

	onSubmit(e){
		e.preventDefault();		
		const item = {
			name: this.refs.item.value,
			done: false
		};
		this.props.addItem(item);
		document.getElementById('input').value = '';
	}
  
	render(){
		return(
			<form id="add-todo" onSubmit={this.onSubmit}>
				<input id="input" className="add" type="text" name="todo" placeholder="What do you need to do" required ref='item' />

				<input className="submit" type="submit" value="Add New Task"/>
			</form>
		);
	}
}

export default AddItem;