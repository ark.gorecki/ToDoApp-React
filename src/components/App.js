import React from 'react';
import TodoComponent from './TodoComponent';
import AddItem from './AddItem';
import '../css/App.css';

class App extends React.Component {
	constructor() {
		super();

		this.addItem = this.addItem.bind(this);
		this.deleteItem = this.deleteItem.bind(this);
		this.toggleComplete = this.toggleComplete.bind(this);

		this.state = {
			todos: []
		};
	}

	addItem(item) {
		const updTodos = this.state.todos;
		updTodos.push(item);
		this.setState({
			todos: updTodos
		});
	}

	deleteItem(item) {
		const updTodos = this.state.todos.filter((val) => {
			return val !== item;
		});
		this.setState({
			todos: updTodos
		});
	}

	toggleComplete(key){
		const todos = this.state.todos;
		const currentState = todos[key].done;
		todos[key].done = !currentState;
		this.setState({ todos });
	}

	render() {
		let todos = this.state.todos;
		todos = todos.map((item, index) => {
			return (
				<TodoComponent 
					item={item} 
					index={index}
					key={index}
					deleteItem={this.deleteItem} 
					toggleComplete={this.toggleComplete}
				/>
			);
		});

		return (
			<div>
				<div className="container-fluid">
					<AddItem addItem={this.addItem} />
					<div className="todo-table">
						<ul>{todos}</ul>
					</div>
				</div>
			</div>
		);
	}
}

export default App;
