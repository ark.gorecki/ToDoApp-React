import React from 'react';

class Header extends React.Component{
	render(){
		return(
			<nav className="navbar">
				<a href='/' alt="Home">Your Simple Todo App</a>
			</nav>
		);
	}
}

export default Header;