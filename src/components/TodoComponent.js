import React from 'react';

class TodoComponent extends React.Component{

	constructor(){
		super();
		this.onDelete = this.onDelete.bind(this);
		this.check = this.check.bind(this);
	}

	onDelete(){
		this.props.deleteItem(this.props.item);
	}
	check(){
		this.props.toggleComplete(this.props.index);
	}

	render(){
		return(
			<li onClick={this.check}>
				<span  className={this.props.item.done ? 'completed': null}>
					{this.props.item.name}
				</span>
				
				<div className='buttonForm'>
					<button className="del" onClick={this.onDelete}><i className="fa fa-trash-o" aria-hidden="true"></i></button>
				</div>
			</li>
		);
	}
}

export default TodoComponent;