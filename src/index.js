import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';

import Header from './components/Header';
import './css/index.css';

import AppComponent from './components/App';


import registerServiceWorker from './registerServiceWorker';

class App extends React.Component{
	render(){
		return(
			<div>
				<BrowserRouter>
					<div>
						<Header/>
						<Route path='/' component={AppComponent} />
					</div>
				</BrowserRouter>
			</div>
		);
	}
}



ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
